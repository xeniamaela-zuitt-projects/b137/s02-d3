package b137.abano.s02d3;

import java.util.HashMap;

public class HashMaps {
    public static void main (String[] args) {
        System.out.println("HashMaps\n");

        //key-value pair data type
        //we can access those items using their indices

        //Syntax
        //HashMap<key_data_type, value_data_type><identifier> = new HashMap<key_data_type, value_data_type><identifier>();

        HashMap<String, String> jobPositions = new HashMap<>();

        //add new elements using PUT method

        jobPositions.put("John", "Team Leader");
        System.out.println(jobPositions);

        jobPositions.put("Paul", "Assistant Team Leader");
        System.out.println(jobPositions);

        //retrieve item via key
        System.out.println(jobPositions.get("John"));

        //retrieve list of keys
        System.out.println(jobPositions.keySet());

        //retrieve list of values
        System.out.println(jobPositions.values());

        //removing an existing item
        jobPositions.remove("John");
        System.out.println(jobPositions);
    }
}
